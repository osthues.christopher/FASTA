package com.pangea.fasta;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class App {
	
	private static String LEVEL = "../";
	
	public static void writeMatches(String path, GenomeMatch genomeMatch) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				String fileName = genomeMatch.getRefFileName();
				int pathSep = fileName.lastIndexOf(File.pathSeparator);
				if (pathSep != -1) {
					fileName = fileName.substring(pathSep + 1);
				}
				fileName = fileName.substring(0, fileName.lastIndexOf("."));
				File file = new File(path);
				file.mkdirs();
				file = new File(path + fileName + ".txt");
				try (FileWriter fw = new FileWriter(file)) {
					fw.write(genomeMatch.toString());
				} catch (IOException e) {
					e.printStackTrace();
				}
				System.out.println("Files has been writen to: " + path + fileName + ".txt");
			}
		}).start();
	}
	
	public static void tinyTestFiles() {
		System.out.println("Begin tiny fasta files");
		String[] fileNames = {"tiny1.fa", "tiny2.fa", "tiny3.fa"};
		String[][] patterns = new String[fileNames.length][4];
		int[][] distances = new int[fileNames.length][4];
		patterns[0] = new String[] {"AAAAA", "AAAAC", "AAAAC", "AAACC"};
		distances[0] = new int[] { 0, 0, 1, 1 };
		patterns[1] = new String[] {"CCCCCC", "ACGTC", "ACGTC", "ACGTC"};
		distances[1] = new int[] { 0, 0, 3, 2 };
		patterns[2] = new String[] {"CGTAC", "ACGTC", "ACGTTT", "ACGTTT"};
		distances[2] = new int[] { 0, 0, 2, 1 };
		int index = 0;
		for (String fileName : fileNames) {
			System.out.println("Computing " + fileName + "...");
			List<String> sequences = FASTAReader.readFasta(fileName);
			GenomeMatch gm = PatternFinder.findPatterns(fileName, patterns[index], distances[index], sequences);
			System.out.println(TestMatches.test(gm, sequences));
			writeMatches("tiny/", gm);
			index++;
		}
		System.out.println("Tiny fasta files done");
	}
	
	public static void smallTestFiles() {
		System.out.println("Begin small fasta files");
		String[] fileNames = {"abi_def.fa"};
		String[][] patterns = new String[fileNames.length][3];
		int[][] distances = new int[fileNames.length][3];
		patterns[0] = new String[] {"TTTTT", "TTGCACG", "TTGCACGC"};
		distances[0] = new int[] { 0, 0, 0 };
		int index = 0;
		for (String fileName : fileNames) {
			System.out.println("Computing " + fileName + "...");
			List<String> sequences = FASTAReader.readFasta(fileName);
			GenomeMatch gm = PatternFinder.findPatterns(fileName, patterns[index], distances[index], sequences);
			System.out.println(TestMatches.test(gm, sequences));
			writeMatches("small/", gm);
			// writeMatches("small/", PatternFinder.findPatterns(fileName, patterns[index++], FASTAReader.readFasta(fileName)));
			index++;
		}
//		List<String> sequences = FASTAReader.readFasta(fileNames[0]);
//		for (String seq : sequences) {
//			System.out.println(seq);
//		}
		System.out.println("Small fasta files done");
	}
	
	public static void mediumTestFiles() {
		System.out.println("Begin medium fasta files");
		String dir = "acetobacter_pasteurianus/";
		String[] fileNames = {"fa.fa", 
				dir + "Acetobacter_pasteurianus_ifo_3283_01.ASM1082v1.dna.chromosome.Chromosome.fa", 
				dir + "Acetobacter_pasteurianus_ifo_3283_03.ASM1084v1.dna.chromosome.Chromosome.fa", 
				dir + "Acetobacter_pasteurianus_ifo_3283_07.ASM1086v1.dna.chromosome.Chromosome.fa", 
				dir + "Acetobacter_pasteurianus_ifo_3283_12.ASM1096v1.dna.chromosome.Chromosome.fa", 
				dir + "Acetobacter_pasteurianus_ifo_3283_22.ASM1088v1.dna.chromosome.Chromosome.fa", 
				dir + "Acetobacter_pasteurianus_ifo_3283_26.ASM1090v1.dna.chromosome.Chromosome.fa", 
				dir + "Acetobacter_pasteurianus_ifo_3283_32.ASM1092v1.dna.chromosome.Chromosome.fa"
				};
		String[][] patterns = new String[fileNames.length][3];
		int[][] distances = new int[fileNames.length][3];
		patterns[0] = new String[] {"TTTTT", "TTGCACGC", "GCGCTTACGT"};
		distances[0] = new int[] { 0, 0, 0 };
		patterns[1] = new String[] {"TTTACG", "TTGCACGCGTGCT", "TTGCACGCGTGCTA"};
		distances[1] = new int[] { 0, 0, 0 };
		patterns[2] = new String[] {"TTTACG", "TTGCACGCGTGCT", "TTGCACGCGTGCTA"};
		distances[2] = new int[] { 0, 0, 0 };
		patterns[3] = new String[] {"TTTACG", "TTGCACGCGTGCT", "TTGCACGCGTGCTA"};
		distances[3] = new int[] { 0, 0, 0 };
		patterns[4] = new String[] {"TTTACG", "TTGCACGCGTGCT", "TTGCACGCGTGCTA"};
		distances[4] = new int[] { 0, 0, 0 };
		patterns[5] = new String[] {"TTTACG", "TTGCACGCGTGCT", "TTGCACGCGTGCTA"};
		distances[5] = new int[] { 0, 0, 0 };
		patterns[6] = new String[] {"TTTACG", "TTGCACGCGTGCT", "TTGCACGCGTGCTA"};
		distances[6] = new int[] { 0, 0, 0 };
		patterns[7] = new String[] {"TTTACG", "TTGCACGCGTGCT", "TTGCACGCGTGCTA"};
		distances[7] = new int[] { 0, 0, 0 };
		int index = 0;
		for (String fileName : fileNames) {
			System.out.println("Computing " + fileName + "...");
			List<String> sequences = FASTAReader.readFasta(fileName);
			GenomeMatch gm = PatternFinder.findPatterns(fileName, patterns[index], distances[index], sequences);
			System.out.println(TestMatches.test(gm, sequences));
			writeMatches("medium/", gm);
			// writeMatches("medium/", PatternFinder.findPatterns(fileName, patterns[index++], FASTAReader.readFasta(fileName)));
			index++;
		}
		System.out.println("Medium fasta files done");
	}
	
	public static void realTestFiles() {
		System.out.println("Begin real fasta files");
		String[] fileNames = {
				"ref_22.fa",
				"22_HG00099.fa",
				"22_HG00513.fa",
				"22_HG01112.fa",
				"22_NA18535.fa",
				"22_NA20800.fa"
				};
		String[][] patterns = new String[fileNames.length][3];
		int[][] distances = new int[fileNames.length][3];
		patterns[0] = new String[] {"AGGTCGCGC", "GTAGTCGTACC", "CGTGGTGACTGATC"};
		distances[0] = new int[] { 0, 0, 0 };
		patterns[1] = new String[] {"AGGTCGCGC", "GTAGTCGTACC", "CGTGGTGACTGATC"};
		distances[1] = new int[] { 0, 0, 0 };
		patterns[2] = new String[] {"AGGTCGCGC", "GTAGTCGTACC", "CGTGGTGACTGATC"};
		distances[2] = new int[] { 0, 0, 0 };
		patterns[3] = new String[] {"AGGTCGCGC", "GTAGTCGTACC", "CGTGGTGACTGATC"};
		distances[3] = new int[] { 0, 0, 0 };
		patterns[4] = new String[] {"AGGTCGCGC", "GTAGTCGTACC", "CGTGGTGACTGATC"};
		distances[4] = new int[] { 0, 0, 0 };
		patterns[5] = new String[] {"AGGTCGCGC", "GTAGTCGTACC", "CGTGGTGACTGATC"};
		distances[5] = new int[] { 0, 0, 0 };
		int index = 0;
		for (String fileName : fileNames) {
			System.out.println("Computing " + fileName + "...");
			List<String> sequences = FASTAReader.readFasta(fileName);
			GenomeMatch gm = PatternFinder.findPatterns(fileName, patterns[index], distances[index], sequences);
			System.out.println(TestMatches.test(gm, sequences));
			writeMatches("real/", gm);
			// writeMatches("medium/", PatternFinder.findPatterns(fileName, patterns[index++], FASTAReader.readFasta(fileName)));
			index++;
		}
		System.out.println("Real fasta files done");
	}
	
	public static void chr1TestFiles() {
		System.out.println("Begin chr1 fasta files");
		String[] fileNames = {
				"1_HG00099.fa",
				"1_HG00110.fa",
				"1_HG00113.fa",
				"1_HG00114.fa",
				"1_HG00120.fa",
				"1_HG00122.fa",
				"1_HG00123.fa",
				"1_HG00125.fa",
				"1_HG00128.fa",
				"1_HG00129.fa",
				"1_HG00135.fa",
				"1_HG00142.fa",
				"1_HG00159.fa",
				"1_HG00173.fa",
				"1_HG00177.fa",
				"1_HG00178.fa",
				"1_HG00179.fa",
				"1_HG00180.fa",
				"1_HG00183.fa",
				"1_HG00188.fa",
				"1_HG00189.fa",
				"1_HG00233.fa",
				"1_HG00234.fa",
				"1_HG00236.fa",
				"1_HG00238.fa",
				"1_HG00240.fa",
				"1_HG00253.fa",
				"1_HG00403.fa",
				"1_HG00418.fa",
				"1_HG00427.fa"
				};
		String[][] patterns = new String[fileNames.length][3];
		int[][] distances = new int[fileNames.length][3];
		for (int i = 0; i < fileNames.length; i++) {
			patterns[i] = new String[] {"AGGTCGCGC", "AGGTCGCGCGAG", "AGGTCGCGCGAGC"};
			distances[i] = new int[] {0, 0, 0};
		}
		int index = 0;
		
		String dir = "chr1/";
		for (String fileName : fileNames) {
			System.out.println("Computing " + fileName + "...");
			List<String> sequences = FASTAReader.readFasta(dir + fileName);
			GenomeMatch gm = PatternFinder.findPatterns(fileName, patterns[index], distances[index], sequences);
			System.out.println(TestMatches.test(gm, sequences));
			writeMatches("chr1/", gm);
			// writeMatches("medium/", PatternFinder.findPatterns(fileName, patterns[index++], FASTAReader.readFasta(fileName)));
			index++;
		}
		System.out.println("chr1 fasta files done");
	}
	
	public static void main(String[] args) {
//		tinyTestFiles();
//		smallTestFiles();
//		mediumTestFiles();
//		realTestFiles();
		chr1TestFiles();
	}
}
