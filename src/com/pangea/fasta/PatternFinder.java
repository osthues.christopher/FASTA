package com.pangea.fasta;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class PatternFinder {
	public static GenomeMatch findPatterns(String fileName, String[] patterns, int[] editDistances, List<String> sequences) {
		GenomeMatch gm = new GenomeMatch();
		gm.setRefFileName(fileName);
		
		for (int i = 0; i < patterns.length; i++) {
			String pattern = patterns[i];
			int editDistance = editDistances[i];
			System.out.println("Searching pattern \"" + pattern + "\"");
			gm.addPatternMatch(findPattern(pattern, editDistance, sequences));
		}
		return gm;
	}
	
	private static PatternMatch findPattern(String pattern, int editDistance, List<String> sequences) {
		if (sequences.size() == 1 && sequences.get(0).length() > 100) {
			return findPatternMultiThread(pattern, editDistance, sequences.get(0));
		} else {
			PatternMatch pm = new PatternMatch();
			pm.setPattern(pattern);
			pm.setEditDistance(editDistance);
			int id = 0;
			List<Thread> threads = new ArrayList<>();

			Set<String> distancePatterns = getAllPatternsWithEditDistance(pattern, editDistance);
			for (String sequence : sequences) {
				final int seqId = id;
				Runnable r = new Runnable() {
					@Override
					public void run() {
						SequenceMatch sm = new SequenceMatch();
						sm.setSequenceId(seqId);
						for (String distancePattern : distancePatterns) {
							int lastIndex = 0;
							int pLength = distancePattern.length();
		
							while (lastIndex != -1) {
							    lastIndex = sequence.indexOf(distancePattern, lastIndex);
		
							    if (lastIndex != -1){
							    	System.out.println("Match found at: " + lastIndex);
							    	sm.addPosition(new Position(lastIndex, pLength));
							        lastIndex += 1;
							    }
							}
						}
						if (!sm.getPositions().isEmpty()) {
							System.out.println("Sort matches.");
							sm.getPositions().sort(new Comparator<Position>() {
								@Override
								public int compare(Position pos0, Position pos1) {
									int compare = Integer.compare(pos0.getStart(), pos1.getStart());
									compare = (0 == compare) ? Integer.compare(pos0.getLength(), pos1.getLength()) : compare;
									return compare;
								}
							});
							synchronized (pm) {
								pm.addSequenceMatch(sm);
							}
						}
					}
				};
				Thread thread = new Thread(r);
				threads.add(thread);
	//			while (threads.size() >= Integer.parseInt(System.getProperty("cores")));
				thread.start();
				while (thread.isAlive());
				synchronized (threads) {
					threads.remove(thread);
				}
				id++;
			}
			System.out.println("Search started for all sequences.");
			while (!threads.isEmpty());
			System.out.println("All sequences searched. Sorting sequences.");
			pm.getSequenceMatch().sort(new Comparator<SequenceMatch>() {
				@Override
				public int compare(SequenceMatch sm0, SequenceMatch sm1) {
					return Integer.compare(sm0.getSequenceId(), sm1.getSequenceId());
				}
			});
			System.out.println("Sequences sorted.");
			return pm;
		}
	}
	
	private static PatternMatch findPatternMultiThread(String pattern, int editDistance, String sequence) {
		PatternMatch pm = new PatternMatch();
		pm.setPattern(pattern);
		pm.setEditDistance(editDistance);
		List<Thread> threads = new ArrayList<>();
		SequenceMatch sm = new SequenceMatch();
		sm.setSequenceId(0);
		if (sequence.length() > 100) {
			Set<String> distancePatterns = getAllPatternsWithEditDistance(pattern, editDistance);
			for (String distancePattern : distancePatterns) {
				int pLength = distancePattern.length();
				
				int splitLength = sequence.length() / 5;
				String[] split = new String[5];
				split[0] = sequence.substring(0, splitLength + 1);
				split[1] = sequence.substring(splitLength, (splitLength + 1) * 2 + distancePattern.length());
				split[2] = sequence.substring(splitLength * 2, (splitLength + 1) * 3 + distancePattern.length());
				split[3] = sequence.substring(splitLength * 3, (splitLength + 1) * 4 + distancePattern.length());
				split[4] = sequence.substring(splitLength * 4);
				for (int i = 0; i < split.length; i++) {
					final int index = i;
					String seq = split[i];
					Runnable r = new Runnable() {
						@Override
						public void run() {
							int lastIndex = 0;
		
							while (lastIndex != -1) {
							    lastIndex = seq.indexOf(distancePattern, lastIndex);
		
							    if (lastIndex != -1){
							    	System.out.println("Match found at: " + lastIndex);
							    	synchronized (sm) {
							    		sm.addPosition(new Position(splitLength * index + lastIndex, pLength)); // TODO(Chris): Compute real start position
							    		lastIndex += 1;
									}
							    }
							}
						}
					};
					Thread thread = new Thread(r);
					threads.add(thread);
		//			while (threads.size() >= Integer.parseInt(System.getProperty("cores")));
					thread.start();
					while (thread.isAlive());
					synchronized (threads) {
						threads.remove(thread);
					}
				}
				while (!threads.isEmpty());
			}
			System.out.println("All subsequences searched. Sorting matches.");
			sm.getPositions().sort(new Comparator<Position>() {
				@Override
				public int compare(Position arg0, Position arg1) {
					return Integer.compare(arg0.getStart(), arg1.getStart());
				}
			});
			if (!sm.getPositions().isEmpty()) {
				System.out.println("Unique matches.");
				Iterator<Position> iter = sm.getPositions().iterator();
				Position prev = iter.next();
				while (iter.hasNext()) {
					Position n = iter.next();
					if (prev.equals(n)) {
						iter.remove();
					}
					prev = n;
				}
				System.out.println("Unique done.");
				pm.addSequenceMatch(sm);
			}
		}
		System.out.println("All matches found.");
		return pm;
	}
	
	private static String ALPHABET = "ACTGN";
	private static Set<String> getAllPatternsWithEditDistance(String pattern, int editDistance) {
		Set<String> distancePatterns = new HashSet<>();
		distancePatterns.add(pattern);
		if (editDistance == 0) {
			return distancePatterns;
		}
		
		for (int patternSplit = 0; patternSplit < pattern.length(); patternSplit++) {
			String beforeSplit = pattern.substring(0, patternSplit);
			String afterSplit = pattern.substring(patternSplit+1);
			
			String deletetPattern = beforeSplit + afterSplit;
			distancePatterns.addAll(getAllPatternsWithEditDistance(deletetPattern, editDistance-1));
			
			for (int i = 0, n = ALPHABET.length(); i < n; i++) {
				char c = ALPHABET.charAt(i);
				String replacedPattern = beforeSplit + c + afterSplit;
				distancePatterns.addAll(getAllPatternsWithEditDistance(replacedPattern, editDistance-1));
				String insertedPattern = beforeSplit + pattern.charAt(patternSplit) + c + afterSplit;
				distancePatterns.addAll(getAllPatternsWithEditDistance(insertedPattern, editDistance-1));
			}
		}
		
		return distancePatterns;
	}
}
