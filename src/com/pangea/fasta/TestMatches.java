package com.pangea.fasta;

import java.util.List;

public class TestMatches {
	public static boolean test(GenomeMatch gm, List<String> sequences) {
		List<PatternMatch> pms = gm.getPatternMatches();
		for (PatternMatch pm : pms) {
			String pattern = pm.getPattern();
			List<SequenceMatch> sms = pm.getSequenceMatch();
			for (SequenceMatch sm : sms) {
				String sequence = sequences.get(sm.getSequenceId());
				List<Position> positions = sm.getPositions();
				for (Position pos : positions) {
					if (!sequence.regionMatches(pos.getStart(), pattern, 0, pos.getLength())) {
						return false;
					}
				}
			}
		}
		return true;
	}
}
