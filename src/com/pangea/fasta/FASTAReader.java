package com.pangea.fasta;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FASTAReader {
	public static List<String> readFasta(String fileName) {
		List<String> sequences = new ArrayList<>();
		try (BufferedReader br = new BufferedReader(new FileReader("0_fa/" + fileName))) {
			System.out.println("Reading in " + fileName + "...");
			String line = null;
			StringBuilder sb = new StringBuilder();
//			String sequence = "";
			while ((line = br.readLine()) != null) {
				if (!line.startsWith(">")) {
//					System.out.println(line);
					sb.append(line);
//					sequence += line;
				} else {
					String sequence = sb.toString();
					if (!sequence.isEmpty()) {
						sequences.add(sequence);
						sb = new StringBuilder();
//						sequence = "";
						System.out.println();
					}
				}
			}
			sequences.add(sb.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Read in " + sequences.size() + " sequences.");
		return sequences;
	}
}	
