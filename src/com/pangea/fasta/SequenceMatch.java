package com.pangea.fasta;

import java.util.ArrayList;
import java.util.List;

public class SequenceMatch {
	private int sequenceId;
	private List<Position> positions;
	
	public SequenceMatch() {
		positions = new ArrayList<>();
	}

	public int getSequenceId() {
		return sequenceId;
	}

	public void setSequenceId(int sequenceId) {
		this.sequenceId = sequenceId;
	}

	public List<Position> getPositions() {
		return positions;
	}

	public void setPositions(List<Position> positions) {
		this.positions = positions;
	}
	
	public void addPosition(Position position) {
		if (!positions.contains(position)) {
			positions.add(position);
		}
	}
	
	@Override
	public String toString() {
		StringBuilder pos = new StringBuilder(sequenceId + ":");
		int index = 0;
		for (Position position : positions) {
			pos.append(position.toString());
			if (index < positions.size() - 1) {
				pos.append(",");
			}
			index++;
		}
		return pos.toString() + "\n";
	}
}
