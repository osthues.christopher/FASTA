package com.pangea.fasta;

import java.util.ArrayList;
import java.util.List;

public class PatternMatch {
	private String pattern;
	private int editDistance = 0;
	private List<SequenceMatch> sequenceMatches;
	
	public PatternMatch() {
		sequenceMatches = new ArrayList<>();
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}
	
	public int getEditDistance() {
		return editDistance;
	}
	
	public void setEditDistance(int editDistance) {
		this.editDistance = editDistance;
	}

	public List<SequenceMatch> getSequenceMatch() {
		return sequenceMatches;
	}

	public void setSequenceMatch(List<SequenceMatch> sequenceMatch) {
		this.sequenceMatches = sequenceMatch;
	}
	
	public void addSequenceMatch(SequenceMatch sequenceMatch) {
		sequenceMatches.add(sequenceMatch);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("?");
		if (editDistance > 0) {
			sb.append("~" + String.valueOf(editDistance) + "~");
		}
		sb.append(pattern + "\n");
		if (sequenceMatches.isEmpty()) {
			sb.append("-\n");
		} else {
			for (SequenceMatch sequenceMatch : sequenceMatches) {
				sb.append(sequenceMatch.toString());
			}
		}
		return sb.toString() + "\n";
	}
}
