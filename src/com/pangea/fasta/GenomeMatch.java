package com.pangea.fasta;

import java.util.ArrayList;
import java.util.List;

public class GenomeMatch {
	private String refFileName;
	private List<PatternMatch> patternMatches;
	
	public GenomeMatch() {
		patternMatches = new ArrayList<>();
	}
	
	public String getRefFileName() {
		return refFileName;
	}

	public void setRefFileName(String refFileName) {
		this.refFileName = refFileName;
	}

	public List<PatternMatch> getPatternMatches() {
		return patternMatches;
	}

	public void setPatternMatches(List<PatternMatch> patternMatches) {
		this.patternMatches = patternMatches;
	}

	public void addPatternMatch(PatternMatch patternMatch) {
		patternMatches.add(patternMatch);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("*" + refFileName + "\n");
		for (PatternMatch patternMatch : patternMatches) {
			sb.append(patternMatch.toString());
		}
		return sb.toString();
	}
}
